package com.fast0n.testdb;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.fast0n.testdb.database.DatabaseHelper;
import com.fast0n.testdb.database.Record;
import com.fast0n.testdb.database.RecordsAdapter;
import com.fast0n.testdb.database.RecyclerItemListener;

public class MainActivity extends AppCompatActivity {
    private RecordsAdapter mAdapter;
    private List<Record> recordsList = new ArrayList<>();
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private TextView textView;

    private DatabaseHelper db;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout = findViewById(R.id.coordinator_layout);
        recyclerView = findViewById(R.id.recycler_view);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);

        db = new DatabaseHelper(this);
        recordsList.addAll(db.getAllRecords());
        mAdapter = new RecordsAdapter(this, recordsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        toggleEmptyRecords();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Random rand = new Random();
                int n = rand.nextInt(3);

                try {
                    createRecord(button.getText().toString() + " " + n);
                } catch (Exception ignored) {
                }

            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemListener(this, recyclerView, new RecyclerItemListener.ClickListener() {
                    @Override
                    public void onClick(View view, final int position) {
                        showActionsDialog(position);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    public void createRecord(String record) {
        long id = db.insertRecord(record);
        Record n = db.getRecord(id);

        if (n != null) {
            recordsList.add(0, n);
            mAdapter.notifyDataSetChanged();

            toggleEmptyRecords();
        }
    }

    private void deleteRecord(int position) {
        db.deleteRecord(recordsList.get(position));
        recordsList.remove(position);
        mAdapter.notifyItemRemoved(position);

        toggleEmptyRecords();
    }

    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[] { "Delete" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    deleteRecord(position);
                }
            }
        });
        builder.show();
    }

    private void toggleEmptyRecords() {
        if (db.getRecordsCount() > 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }
    }
}
