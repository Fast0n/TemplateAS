public class Model {

    public String nome;
    public String apertura;
    public String valutazione;
    public String indirizzo;

    public Model(String nome, String apertura, String valutazione, String indirizzo) {
        this.nome = nome;
        this.apertura = apertura;
        this.valutazione = valutazione;
        this.indirizzo = indirizzo;
    }
}
