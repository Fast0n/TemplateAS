import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Model> modelList = new ArrayList<>();
    RecyclerView recycler_view;
    TextView getNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recycler_view = findViewById(R.id.recycler_view);

        recycler_view.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(llm);
        String url = "http://android12.altervista.org/API/palizzi";
        createList(url);

        recycler_view.addOnItemTouchListener(new RecyclerItemListener(getApplicationContext(), recycler_view,
                new RecyclerItemListener.RecyclerTouchListener() {
                    public void onClickItem(View arg1, int position) {
                        getNome = arg1.findViewById(R.id.nome);
                        String nome = getNome.getText().toString();
                        Toast.makeText(MainActivity.this, nome, Toast.LENGTH_LONG).show();
                    }

                    public void onLongClickItem(View v, int position) {
                    }
                }));

    }

    private void createList(String url) {

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            JSONObject json_raw = new JSONObject(response.toString());
                            String lista = json_raw.getString("lista");
                            JSONArray arraylista = new JSONArray(lista);

                            int nElementi = Integer.parseInt(String.valueOf(arraylista.length())) - 1;

                            for (int i = 0; i < nElementi; i++) {
                                String ristoranti = arraylista.getString(i);

                                JSONObject scorroRistoranti = new JSONObject(ristoranti);
                                String nome = scorroRistoranti.getString("nome");
                                String apertura = scorroRistoranti.getString("apertura").replace("null",
                                        "Apertura non disponibile");
                                String valutazione = scorroRistoranti.getString("valutazione");
                                String indirizzo = scorroRistoranti.getString("indirizzo");

                                modelList.add(new Model(nome, apertura, valutazione, indirizzo));
                            }
                            ModelAdapter ca = new ModelAdapter(modelList);
                            recycler_view.setAdapter(ca);

                        } catch (JSONException ignored) {
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int error_code = error.networkResponse.statusCode;

                    }
                });

        // add it to the RequestQueue
        queue.add(getRequest);

    }
}
