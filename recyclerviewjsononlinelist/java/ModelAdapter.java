import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.MyViewHolder> {

    private List<Model> modelList;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView nome, apertura, valutazione, indirizzo;

        public MyViewHolder(View view) {
            super(view);
            nome = view.findViewById(R.id.nome);
            apertura = view.findViewById(R.id.apertura);
            valutazione = view.findViewById(R.id.valutazione);
            indirizzo = view.findViewById(R.id.indirizzo);
        }
    }

    public ModelAdapter(List<Model> modelList) {
        this.modelList = modelList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model c = modelList.get(position);
        holder.nome.setText(c.nome);
        holder.apertura.setText(c.apertura);
        holder.valutazione.setText(c.valutazione);
        holder.indirizzo.setText(c.indirizzo);
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new MyViewHolder(v);
    }
}
